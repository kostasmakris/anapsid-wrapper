/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.anapsid.wrapper;

import junit.framework.TestCase;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class AnapsidWrapperTest extends TestCase {

    public AnapsidWrapperTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of executeQuery method, of class AnapsidWrapper.
     */
    public void testExecuteQuery() {
        System.out.println("executeQuery");

        String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
                + "PREFIX ne: <http://www.natural-europe.eu/ontology#> "
                + "PREFIX dbpedia: <http://dbpedia.org/ontology/> "
                + "SELECT * "
                + "WHERE { "
                //                + "   {SERVICE <http://62.217.125.111:8890/sparql> {?x rdfs:seeAlso ?y}} "
                + "   {SERVICE <http://dbpedia.org/sparql> {?y dbpedia:conservationStatus ?z}} "
                + "}";

        AnapsidWrapper anapsid = new AnapsidWrapper("/home/kostas/programs/anapsid-3.0/endpointsDescriptions.txt");
        ResultSet rs = anapsid.executeQuery(query);
    }

}
