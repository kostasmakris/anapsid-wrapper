/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.anapsid.wrapper;

import static gr.tuc.music.anapsid.wrapper.StreamReader.Type.ERROR;
import static gr.tuc.music.anapsid.wrapper.StreamReader.Type.OUTPUT;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class StreamReader extends Thread {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(StreamReader.class);

    private final InputStream is;
    private final Type type;
    private final OutputStream os;

    public StreamReader(InputStream is, Type type) {
        this(is, type, null);
    }

    public StreamReader(InputStream is, Type type, OutputStream os) {
        this.is = is;
        this.type = type;
        this.os = os;
    }

    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String line;
            while ((line = br.readLine()) != null) {
                if ((os != null) && (type == OUTPUT)) {
                    JsonFactory factory = new JsonFactory();
                    ObjectMapper mapper = new ObjectMapper(factory);
                    mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

                    TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
                    };

                    try {
                        HashMap<String, String> result = mapper.readValue(line, typeRef);
                        String json = mapper.writeValueAsString(result);

                        os.write((json + "\n").getBytes("UTF-8"));
                    } catch (IOException ex) {
                        log.info(line);
                    }
                } else if (type == ERROR) {
                    log.error(line);
                } else {
                    log.info(line);
                }
            }
        } catch (IOException ex) {
            log.warn("IO Exception occured.", ex);
            throw new RuntimeException(ex);
        } finally {
            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(is);
        }
    }

    public enum Type {

        OUTPUT, ERROR;
    }
}
