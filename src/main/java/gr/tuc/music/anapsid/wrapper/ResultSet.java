/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.anapsid.wrapper;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.LoggerFactory;

/**
 * A result set.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ResultSet implements Iterable<QuerySolution>, Closeable {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ResultSet.class);

    private final List<String> varList;
    private final File file;

    /**
     * Creates a ResultSet object.
     *
     * @param file The file containing the query solutions.
     */
    public ResultSet(File file) {
        this(new ArrayList<String>(), file);
    }

    /**
     * Creates a ResultSet object.
     *
     * @param varList The list of query solution variables.
     * @param file    The file containing the query solutions.
     */
    public ResultSet(List<String> varList, File file) {
        this.varList = new ArrayList<String>(varList);
        this.file = file;
    }

    public List<String> getVarList() {
        return varList;
    }

    public String getFilename() {
        return file.getName();
    }

    /**
     * Prints the query solutions to the standard output.
     */
    public void print() {
        // print the query solutions in json style
        for (QuerySolution solution : this) {
            boolean isFirst = true;

            System.out.print("{");
            for (String var : varList) {
                String value = solution.get(var);

                if (isFirst) {
                    isFirst = false;
                } else {
                    System.out.print(", ");
                }

                if (value != null) {
                    System.out.print("\"" + var + "\": \"" + value + "\"");
                } else {
                    System.out.print("\"" + var + "\": ");
                }
            }
            System.out.println("}");
        }
    }

    /**
     * Returns the number of query solutions.
     *
     * @return The number of query solutions.
     */
    public int size() {
        int counter = 0;

        for (QuerySolution solution : this) {
            counter = counter + 1;
        }

        return counter;
    }

    public File toJSON() {
        File jsonFile = null;
        OutputStream os = null;

        try {
            boolean isFirst = true;
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);

            String id = UUID.randomUUID().toString();
            jsonFile = File.createTempFile(id, ".tmp");
            jsonFile.deleteOnExit();

            os = FileUtils.openOutputStream(jsonFile);
            os.write(("[").getBytes("UTF-8"));

            for (QuerySolution solution : this) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    os.write((", ").getBytes("UTF-8"));
                }

                os.write(mapper.writeValueAsBytes(solution.getVarValueMap()));
            }
            os.write(("]").getBytes("UTF-8"));
        } catch (IOException ex) {
            log.warn("IO Exception occured.", ex);
        } finally {
            IOUtils.closeQuietly(os);
        }

        return jsonFile;
    }

    public Iterator<QuerySolution> iterator() {
        try {
            return new QuerySolutionIterator();
        } catch (IOException ex) {
            log.warn("IO Exception occured.", ex);
        }

        return new ArrayList<QuerySolution>().iterator();
    }

    public void close() throws IOException {
        // do nothing
    }

    private class QuerySolutionIterator implements Iterator<QuerySolution> {

        private final LineIterator lineIterator;
        private QuerySolution nextSolution;

        public QuerySolutionIterator() throws IOException {
            lineIterator = FileUtils.lineIterator(file, "UTF-8");
            nextSolution = null;
        }

        public boolean hasNext() {
            while ((nextSolution == null) && lineIterator.hasNext()) {
                String line = lineIterator.next();

                JsonFactory factory = new JsonFactory();
                ObjectMapper mapper = new ObjectMapper(factory);
                mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

                TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
                };

                try {
                    HashMap<String, String> value = mapper.readValue(line, typeRef);
                    nextSolution = new QuerySolution(value);
                } catch (IOException ex) {
                    // query solution failed to be parsed
                    log.info(line);
                }
            }

            return (nextSolution != null);
        }

        public QuerySolution next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            QuerySolution solution = nextSolution;
            nextSolution = null;

            return solution;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
