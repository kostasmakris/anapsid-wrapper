/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.anapsid.wrapper;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryException;
import com.hp.hpl.jena.query.QueryFactory;
import gr.tuc.music.anapsid.wrapper.StreamReader.Type;
import gr.tuc.music.anapsid.wrapper.utils.arq.serializer.Serializer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.apache.commons.io.FileUtils;
import org.apache.jena.atlas.io.IndentedLineBuffer;
import org.slf4j.LoggerFactory;

/**
 * A Java Wrapper for the Anapsid Federated Query Engine
 * (https://github.com/anapsid/anapsid).
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class AnapsidWrapper {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(AnapsidWrapper.class);

    private String endpointDescLocation;
    private boolean decompEnabled;
    private boolean adaptiveOpsEnabled;
    private boolean costModelEnabled;
    private boolean ansRetrievalEnabled;
    private PlanType planType;
    private DecompositionType decompType;
    private long timeout;

    /**
     * Creates an AnapsidWrapper object using the endpoints file location along
     * with some default values.
     *
     * @param endpointsFile The path and name of the file where the description
     *                      of the endpoints is stored.
     */
    public AnapsidWrapper(String endpointsFile) {
        this.endpointDescLocation = endpointsFile;

        decompEnabled = false;
        adaptiveOpsEnabled = true;
        costModelEnabled = true;
        ansRetrievalEnabled = true;
        planType = PlanType.ll;
        decompType = DecompositionType.SSGM;
        timeout = 1000000;
    }

    public String getEndpointDescLocation() {
        return endpointDescLocation;
    }

    public void setEndpointDescLocation(String endpointDescLocation) {
        this.endpointDescLocation = endpointDescLocation;
    }

    public boolean isDecompEnabled() {
        return decompEnabled;
    }

    public void setDecompEnabled(boolean decompEnabled) {
        this.decompEnabled = decompEnabled;
    }

    public boolean isAdaptiveOpsEnabled() {
        return adaptiveOpsEnabled;
    }

    public void setAdaptiveOpsEnabled(boolean adaptiveOpsEnabled) {
        this.adaptiveOpsEnabled = adaptiveOpsEnabled;
    }

    public boolean isCostModelEnabled() {
        return costModelEnabled;
    }

    public void setCostModelEnabled(boolean costModelEnabled) {
        this.costModelEnabled = costModelEnabled;
    }

    public boolean isAnsRetrievalEnabled() {
        return ansRetrievalEnabled;
    }

    public void setAnsRetrievalEnabled(boolean ansRetrievalEnabled) {
        this.ansRetrievalEnabled = ansRetrievalEnabled;
    }

    public PlanType getPlanType() {
        return planType;
    }

    public void setPlanType(PlanType planType) {
        this.planType = planType;
    }

    public DecompositionType getDecompType() {
        return decompType;
    }

    public void setDecompType(DecompositionType decompType) {
        this.decompType = decompType;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public ResultSet executeQuery(String queryStr) {
        ResultSet resultSet = null;
        List<String> varList;
        File tmpFile = null;
        Query query;

        try {
            // parse the input query
            query = QueryFactory.create(queryStr);

            // fetch the result query variables
            varList = query.getResultVars();

            // re-serialize the query in order to remove any semicolons from
            // basic graph patterns since they are not supported by Anapsid
            IndentedLineBuffer buff = new IndentedLineBuffer();
            Serializer.serialize(query, buff);
            queryStr = buff.toString();
        } catch (QueryException ex) {
            log.warn("Invalid input query.");
            return null;
        }

        try {
            String id = UUID.randomUUID().toString();
            tmpFile = File.createTempFile(id, ".tmp");
            tmpFile.deleteOnExit();

            // store input query to temporary file
            FileUtils.writeStringToFile(tmpFile, queryStr);

            // create query execution command
            String command = createCommand(tmpFile.getAbsolutePath());

            // execute the command and return the result set
            return new ResultSet(varList, exec(command));

        } catch (IOException ex) {
            log.warn("Temporary file failed to be created.", ex);
        } finally {
            if (tmpFile != null) {
                tmpFile.delete();
            }
        }

        return resultSet;
    }

    private String createCommand(String queryLocation) {
        // create query execution command
        String command = "run_anapsid";

        // indicate the path and name of the file where the description of the
        // endpoints is stored
        command = command + " -e " + endpointDescLocation;
        // add the path and name of the file where the query is stored
        command = command + " -q " + queryLocation;
        // indicate the plan type
        command = command + " -p " + planType;
        // indicate -s parameter (not specified in the Anapsid documentation)
        command = command + " -s False";
        // indicate if the input query is SPARQL 1.1 or needs decomposition
        command = command + " -o " + valueOf(!decompEnabled);
        // indicate the type of decomposition
        command = command + " -d " + decompType;
        // indicate if the adaptive operators will be used
        command = command + " -a " + valueOf(adaptiveOpsEnabled);
        // indicate if the cardinality of the queries will be estimated by
        // contacting the sources or by using a cost model
        command = command + " -w " + valueOf(!costModelEnabled);
        // indicate if the answer of the query will be output
        command = command + " -r " + valueOf(ansRetrievalEnabled);

        return command;
    }

    private String valueOf(boolean value) {
        return value ? "True" : "False";
    }

    private File exec(String command) {
        File resultSet = null;

        try {
            String id = UUID.randomUUID().toString();
            resultSet = File.createTempFile(id, ".tmp");
            resultSet.deleteOnExit();

            CommandLine cl = CommandLine.parse(command);

            //create a watchdog with a specific timeout limit
            ExecuteWatchdog watchDog = new ExecuteWatchdog(timeout);

            // result handler for executing the process in an Asynch way
            DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

            // using a file for the output stream (error stream not used)
            PumpStreamHandler streamHandler = new PumpStreamHandler(new FileOutputStream(resultSet), null);

            // use a process destroyer to end the process when the JVM exits
            ShutdownHookProcessDestroyer processDestroyer = new ShutdownHookProcessDestroyer();

            // create the main command executor
            DefaultExecutor executor = new DefaultExecutor();

            // set the executor properties
            executor.setStreamHandler(streamHandler);
            executor.setWatchdog(watchDog);
            executor.setProcessDestroyer(processDestroyer);

            // execute the command
            executor.execute(cl, resultHandler);

            // anything after the above statement will be executed only when the
            // command completes the execution
            resultHandler.waitFor();

            int exitValue = resultHandler.getExitValue();
            if (executor.isFailure(exitValue)) {
                log.warn("Command execution failed maybe due to a timeout.");
            }
        } catch (IOException ex) {
            log.warn("Command failed to be executed properly.", ex);
        } catch (InterruptedException ex) {
            log.warn("Command interrupted.", ex);
        }

        return resultSet;
    }

    private File execRuntime(String command) {
        File resultSet = null;

        Process proc = null;
        try {

            Runtime rt = Runtime.getRuntime();
            proc = rt.exec(command);

            // reader for error messages
            StreamReader errorReader = new StreamReader(proc.getErrorStream(), Type.ERROR);

            String id = UUID.randomUUID().toString();
            resultSet = File.createTempFile(id, ".tmp");
            resultSet.deleteOnExit();

            // reader for output messages
            StreamReader outputReader = new StreamReader(proc.getInputStream(), Type.OUTPUT, new FileOutputStream(resultSet));

            // start the reader threads
            errorReader.start();
            outputReader.start();

            // force the main thread to wait until the readers finish reading
            // input from the streams - otherwise they will be closed automatically
            // upon the completion/interrupt of the initially created process BUT
            // there is no guarrantee that the readers will have finished reading -
            // on process termination any related streams are closed - an IOException
            // is thrown when readers try fetch content from a closed stream
            errorReader.join();
            outputReader.join();

            // create a worker thread for the Anapsid process (in order to put a
            // timeout limit on it)
            Worker worker = new Worker(proc);
            worker.start();

            try {
                // add timeout limit to the Anapsid process and make the current
                // thread to wait for it
                worker.join(timeout);

                Integer exitVal = worker.getExitVal();
                if (exitVal != null) {
                    // the worker thread completed within the timeout period
                    return resultSet;
                }

                log.warn("Command timed out.");
                proc.destroy();
            } catch (InterruptedException ex) {
                proc.destroy();
                worker.interrupt();
                Thread.currentThread().interrupt();
                throw ex;
            }
        } catch (IOException ex) {
            log.warn("Command failed to be executed properly.", ex);
        } catch (InterruptedException ex) {
            log.warn("Command interrupted.", ex);
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }

        return resultSet;
    }

    private static class Worker extends Thread {

        private final Process process;
        private Integer exitVal;

        private Worker(Process process) {
            this.process = process;
        }

        public Integer getExitVal() {
            return exitVal;
        }

        @Override
        public void run() {
            try {
                exitVal = process.waitFor();
            } catch (InterruptedException ex) {
                return;
            }
        }
    }
}
