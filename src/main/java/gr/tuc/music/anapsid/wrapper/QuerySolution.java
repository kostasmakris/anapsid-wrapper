/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.anapsid.wrapper;

import java.util.HashMap;
import java.util.Iterator;

/**
 * A query solution.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class QuerySolution {

    private final HashMap<String, String> varValueMap;

    /**
     * Creates a QuerySolution object.
     *
     * @param varValueMap A hashtable consisted of variable-value pairs.
     */
    public QuerySolution(HashMap<String, String> varValueMap) {
        if (varValueMap != null) {
            this.varValueMap = varValueMap;
        } else {
            this.varValueMap = new HashMap<String, String>();
        }
    }

    public HashMap<String, String> getVarValueMap() {
        return varValueMap;
    }

    /**
     * Returns the value of a given variable in this query solution. A return of
     * null indicates that the variable is not present in this solution.
     *
     * @param varName A variable name.
     * @return The value of a given variable.
     */
    public String get(String varName) {
        return varValueMap.get(varName);
    }

    /**
     * Checks if the query solution contains a value for a given variable name.
     *
     * @param varName A variable name.
     * @return True in case the query solution contains a value for the given
     *         variable name, otherwise false.
     */
    public boolean contains(String varName) {
        return varValueMap.containsKey(varName);
    }

    /**
     * Returns an iterator over the variable names in this query solution.
     *
     * @return A variable name iterator.
     */
    public Iterator<String> varNames() {
        return varValueMap.keySet().iterator();
    }
}
