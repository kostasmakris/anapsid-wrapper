# README #

This is a Java wrapper for ANAPSID, an open source query engine for SPARQL endpoints developed at Universidad Simón Bolívar. ANAPSID has been implemented in Python. It adapts query execution schedulers to data availability and runtime conditions. Moreover, it provides physical SPARQL operators that detect when a source becomes blocked or data traffic is bursty, and opportunistically, the operators produce results as quickly as data arrives from the sources. The implemented Java wrapper encapsulates ANAPSID’s internal functionality and exposes methods for query submission and result set streaming.

*More information about ANAPSID is available at: [https://github.com/anapsid/anapsid](https://github.com/anapsid/anapsid)*